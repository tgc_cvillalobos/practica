package mx.tgc.model.module;

import mx.tgc.model.module.applicationModule.CataBloquesPrediosAMFixture;
import mx.tgc.model.module.applicationModule.CataBloquesPrediosAMTest;
import mx.tgc.model.module.view.PrPredBloquesAgrupacionesVO1VO.PrPredBloquesAgrupacionesVO1VOTest;
import mx.tgc.model.module.view.PrPredBloquesAgrupacionesVVO1VO.PrPredBloquesAgrupacionesVVO1VOTest;

import org.junit.*;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses( { PrPredBloquesAgrupacionesVO1VOTest.class,
                       PrPredBloquesAgrupacionesVVO1VOTest.class,
                       CataBloquesPrediosAMTest.class })
public class AllCataBloquesPrediosAMTests {
    @BeforeClass
    public static void setUp() {
    }

    @AfterClass
    public static void tearDown() throws Exception {
        CataBloquesPrediosAMFixture.getInstance().release();
    }
}
