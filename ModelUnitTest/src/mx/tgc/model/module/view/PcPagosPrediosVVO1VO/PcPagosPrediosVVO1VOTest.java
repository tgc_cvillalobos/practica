package mx.tgc.model.module.view.PcPagosPrediosVVO1VO;

import mx.tgc.model.module.applicationModule.ConsultaAMFixture;

import oracle.jbo.ViewObject;

import org.junit.After;
import static org.junit.Assert.assertNotNull;
import org.junit.Before;
import org.junit.Test;

public class PcPagosPrediosVVO1VOTest {
    private ConsultaAMFixture fixture1 =
        ConsultaAMFixture.getInstance();

    public PcPagosPrediosVVO1VOTest() {
    }

    @Test
    public void testAccess() {
        ViewObject view =
            fixture1.getApplicationModule().findViewObject("PcPagosPrediosVVO1");
        assertNotNull(view);
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }
}
