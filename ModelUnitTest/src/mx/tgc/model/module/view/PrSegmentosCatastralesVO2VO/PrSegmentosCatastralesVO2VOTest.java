package mx.tgc.model.module.view.PrSegmentosCatastralesVO2VO;

import mx.tgc.model.module.applicationModule.CataSegCatastralesAMFixture;

import oracle.jbo.ViewObject;

import org.junit.*;
import static org.junit.Assert.*;

public class PrSegmentosCatastralesVO2VOTest {
    private CataSegCatastralesAMFixture fixture1 = CataSegCatastralesAMFixture.getInstance();

    public PrSegmentosCatastralesVO2VOTest() {
    }

    @Test
    public void testAccess() {
        ViewObject view = fixture1.getApplicationModule().findViewObject("PrSegmentosCatastralesVO2");
        assertNotNull(view);
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }
}