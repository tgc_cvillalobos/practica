package mx.tgc.model.module.view.PcAsentamientosVVO1VO;

import mx.tgc.model.module.applicationModule.CataCaractAsentamientoAMFixture;

import oracle.jbo.ViewObject;

import org.junit.*;
import static org.junit.Assert.*;

public class PcAsentamientosVVO1VOTest {
    private CataCaractAsentamientoAMFixture fixture1 = CataCaractAsentamientoAMFixture.getInstance();

    public PcAsentamientosVVO1VOTest() {
    }

    @Test
    public void testAccess() {
        ViewObject view = fixture1.getApplicationModule().findViewObject("PcAsentamientosVVO1");
        assertNotNull(view);
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }
}
