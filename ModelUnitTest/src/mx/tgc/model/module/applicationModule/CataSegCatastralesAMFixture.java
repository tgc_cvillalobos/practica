package mx.tgc.model.module.applicationModule;

import oracle.jbo.ApplicationModule;
import oracle.jbo.client.Configuration;

public class CataSegCatastralesAMFixture {
    private static CataSegCatastralesAMFixture fixture1 = new CataSegCatastralesAMFixture();
    private ApplicationModule _am;

    private CataSegCatastralesAMFixture() {
        _am = Configuration.createRootApplicationModule("mx.tgc.model.module.CataSegCatastralesAM","CataSegCatastralesAMLocal");
    }

    public void setUp() {
    }

    public void tearDown() {
    }

    public static CataSegCatastralesAMFixture getInstance() {
        return fixture1;
    }

    public void release() throws Exception {
        Configuration.releaseRootApplicationModule(_am, true);
    }

    public ApplicationModule getApplicationModule() {
        return _am;
    }
}
