package mx.tgc.model.module.applicationModule;

import java.sql.SQLException;
import java.sql.Statement;

import mx.tgc.model.module.CataUsoSueloAMImpl;

import mx.tgc.model.view.catastro.PrGirosVOImpl;

import org.junit.*;
import static org.junit.Assert.*;

public class CataUsoSueloAMTest {
    private static CataUsoSueloAMFixture fixture1 =
        CataUsoSueloAMFixture.getInstance();
    private static CataUsoSueloAMImpl _amImpl =
        (CataUsoSueloAMImpl)fixture1.getApplicationModule();

    public CataUsoSueloAMTest() {
    }

    /**
     * M�todo usado para inicializar todo lo necesario para las pruebas.
     *
     * @author Isaac S�as Guti�rrez
     * @since 14-SEP-2011
     */
    @BeforeClass
    public static void setUp() {
        insertGirosPorPadre();

    }

    /**
     * M�todo usado para borrar todo lo creado para las pruebas.
     *
     * @author Isaac S�as Guti�rrez
     * @since 14-SEP-2011
     */
    @AfterClass
    public static void tearDown() {
        deleteGirosPorPadre();
    }

    /**
     * M�todo usado para probar filtrarGirosPorPadre.
     * Ejecuta el m�todo, luego se cuenta el n�mero de registros que deja el
     * filtro, de obtener diferente n�mero del esperado, devolvemos un mensaje
     * indicando cual fue el resultado.
     *
     * @author Isaac S�as Guti�rrez
     * @since 14-SEP-2011
     */
    @Test
    public void testFiltrarGirosPorPadre() {
        _amImpl.filtrarGirosPorPadre(new oracle.jbo.domain.Number(9999),
                                     new oracle.jbo.domain.Number(999999999999999L));
        PrGirosVOImpl girosVO1 = _amImpl.getPrGirosVO1();
        int cantidadGiros = girosVO1.getRowCount();
        assertTrue("La cantidad esperada de giros es 2 y se recibieron: " +
                   cantidadGiros, cantidadGiros == 2);
    }

    @Test
    public void testFiltrarPorRecaudacion() {
    }

    /**
     * M�todo usado para la prueba unitaria de testFiltrarGirosPorPadre, y que
     * inserta los registros necesarios para la prueba.
     *
     * @author Isaac S�as Guti�rrez
     * @since 14-SEP-2011
     */
    private static void insertGirosPorPadre() {
        Statement stmt = _amImpl.getDBTransaction().createStatement(1);
        try {
            stmt.execute("Insert into CC_RECAUDACIONES (IDENTIFICADOR,CLAVE,NOMBRE,ESTATUS,DOMICILIO,RECAUDADOR,CC_RECA_IDENTIFICADOR,CAMPO1,CAMPO2,CAMPO3,CAMPO4,CAMPO5,OPERACION) values (999999999999999,'CLAVE','NOMBRE','AC',null,null,null,null,null,null,null,null,'VN')");
            stmt.execute("Insert into PR_GIROS (SERIE,IDENTIFICADOR,CLAVE,NOMBRE,DESCRIPCION,NIVEL,CC_RECA_IDENTIFICADOR,PR_GIRO_SERIE,PR_GIRO_IDENTIFICADOR,CAMPO1,CAMPO2,CAMPO3,CAMPO4,CAMPO5,CAMPO6,CAMPO7,CAMPO8,CAMPO9,CAMPO10,CAMPO11,CAMPO12,CAMPO13,CAMPO14,CAMPO15,CREADO_POR,CREADO_EL,MODIFICADO_POR,MODIFICADO_EL,ESTATUS) values (9999,999999999999999,'CLAVE9','NOMBRE',null,'1',999999999999999,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,'RECAUDADOR',to_timestamp('26/08/11','DD/MM/RR HH24:MI:SSXFF'),'RECAUDADOR',to_timestamp('26/08/11','DD/MM/RR HH24:MI:SSXFF'),'AC')");
            stmt.execute("Insert into PR_GIROS (SERIE,IDENTIFICADOR,CLAVE,NOMBRE,DESCRIPCION,NIVEL,CC_RECA_IDENTIFICADOR,PR_GIRO_SERIE,PR_GIRO_IDENTIFICADOR,CAMPO1,CAMPO2,CAMPO3,CAMPO4,CAMPO5,CAMPO6,CAMPO7,CAMPO8,CAMPO9,CAMPO10,CAMPO11,CAMPO12,CAMPO13,CAMPO14,CAMPO15,CREADO_POR,CREADO_EL,MODIFICADO_POR,MODIFICADO_EL,ESTATUS) values (9999,999999999999997,'CLAVE7','NOMBRE',null,'3',999999999999999,9999,999999999999999,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,'RECAUDADOR',to_timestamp('26/08/11','DD/MM/RR HH24:MI:SSXFF'),'RECAUDADOR',to_timestamp('26/08/11','DD/MM/RR HH24:MI:SSXFF'),'AC')");
            stmt.execute("Insert into PR_GIROS (SERIE,IDENTIFICADOR,CLAVE,NOMBRE,DESCRIPCION,NIVEL,CC_RECA_IDENTIFICADOR,PR_GIRO_SERIE,PR_GIRO_IDENTIFICADOR,CAMPO1,CAMPO2,CAMPO3,CAMPO4,CAMPO5,CAMPO6,CAMPO7,CAMPO8,CAMPO9,CAMPO10,CAMPO11,CAMPO12,CAMPO13,CAMPO14,CAMPO15,CREADO_POR,CREADO_EL,MODIFICADO_POR,MODIFICADO_EL,ESTATUS) values (9999,999999999999998,'CLAVE8','NOMBRE',null,'2',999999999999999,9999,999999999999999,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,'RECAUDADOR',to_timestamp('26/08/11','DD/MM/RR HH24:MI:SSXFF'),'RECAUDADOR',to_timestamp('26/08/11','DD/MM/RR HH24:MI:SSXFF'),'AC')");
            _amImpl.getDBTransaction().commit();
        } catch (SQLException ex) {
            ex.printStackTrace();
            _amImpl.getDBTransaction().rollback();
        }
    }

    /**
     * M�todo usado para la prueba unitaria de testFiltrarGirosPorPadre, y que
     * borra todo lo creado para la prueba.
     *
     * @author Isaac S�as Guti�rrez
     * @since 14-SEP-2011
     */
    private static void deleteGirosPorPadre() {
        Statement stmt = _amImpl.getDBTransaction().createStatement(1);
        try {
            stmt.executeUpdate("delete from pr_giros where serie = 9999 and identificador in (999999999999998,999999999999997)");
            stmt.executeUpdate("delete from pr_giros where serie = 9999 and identificador = 999999999999999");
            stmt.execute("delete from cc_recaudaciones where identificador = 999999999999999");
            _amImpl.getDBTransaction().commit();
        } catch (SQLException ex) {
            ex.printStackTrace();
            _amImpl.getDBTransaction().rollback();
        }
    }
}