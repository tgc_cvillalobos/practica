package mx.tgc.model.module.applicationModule;

import oracle.jbo.ApplicationModule;
import oracle.jbo.client.Configuration;

public class CataAvaluoAMFixture {
    private static CataAvaluoAMFixture fixture1 = new CataAvaluoAMFixture();
    private ApplicationModule _am;

    private CataAvaluoAMFixture() {
        _am = Configuration.createRootApplicationModule("mx.tgc.model.module.CataAvaluoAM","CataAvaluoAMLocal");
    }

    public void setUp() {
    }

    public void tearDown() {
    }

    public static CataAvaluoAMFixture getInstance() {
        return fixture1;
    }

    public void release() throws Exception {
        Configuration.releaseRootApplicationModule(_am, true);
    }

    public ApplicationModule getApplicationModule() {
        return _am;
    }
}
