package mx.tgc.model.module.applicationModule;

import oracle.jbo.ApplicationModule;
import oracle.jbo.client.Configuration;

public class CataCaracteristicasValuacionAMFixture {
    private static CataCaracteristicasValuacionAMFixture fixture1 = new CataCaracteristicasValuacionAMFixture();
    private ApplicationModule _am;

    private CataCaracteristicasValuacionAMFixture() {
        _am = Configuration.createRootApplicationModule("mx.tgc.model.module.CataCaracteristicasValuacionAM","CataCaractValuacionAMLocal");
    }

    public void setUp() {
    }

    public void tearDown() {
    }

    public static CataCaracteristicasValuacionAMFixture getInstance() {
        return fixture1;
    }

    public void release() throws Exception {
        Configuration.releaseRootApplicationModule(_am, true);
    }

    public ApplicationModule getApplicationModule() {
        return _am;
    }
}
