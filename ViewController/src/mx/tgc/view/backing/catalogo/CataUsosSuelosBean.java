package mx.tgc.view.backing.catalogo;

import java.io.IOException;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.el.ELContext;
import javax.el.ExpressionFactory;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;

import mx.tgc.utilityfwk.resource.PropertiesReader;
import mx.tgc.utilityfwk.resource.PublicKeys;
import mx.tgc.utilityfwk.resource.RowTreeTable;
import mx.tgc.utilityfwk.view.managed.GenericBean;

import oracle.adf.controller.v2.context.LifecycleContext;
import oracle.adf.model.AttributeBinding;
import oracle.adf.model.BindingContext;
import oracle.adf.model.binding.DCBindingContainer;
import oracle.adf.model.binding.DCIteratorBinding;
import oracle.adf.view.rich.component.rich.RichDialog;
import oracle.adf.view.rich.component.rich.RichPopup;
import oracle.adf.view.rich.component.rich.input.RichSelectOneChoice;
import oracle.adf.view.rich.component.rich.layout.RichPanelGroupLayout;
import oracle.adf.view.rich.context.AdfFacesContext;
import oracle.adf.view.rich.event.DialogEvent;
import oracle.adf.view.rich.event.PopupFetchEvent;

import oracle.binding.BindingContainer;
import oracle.binding.OperationBinding;

import oracle.jbo.Row;

import oracle.jbo.uicli.binding.JUCtrlHierBinding;

import oracle.jbo.uicli.binding.JUCtrlHierNodeBinding;

import org.apache.myfaces.trinidad.context.RequestContext;
import org.apache.myfaces.trinidad.model.CollectionModel;
import org.apache.myfaces.trinidad.model.RowKeySet;
import org.apache.myfaces.trinidad.render.ExtendedRenderKitService;
import org.apache.myfaces.trinidad.util.Service;


/**
 * CataUsosSuelosBean
 *
 * Clase encargada del manejo de eventos y validaciones del cat�logo de usos
 * de suelos.
 *
 * @author
 * @components
 * @version 2.0 16/12/2013
 *
 * Tecnolog�a de Gesti�n y Comunicaci�n SA de CV
 *
 * Todos los Derechos Reservados
 * Copyright 2010
 */
public class CataUsosSuelosBean extends GenericBean {
    private RichPopup confirmaPopUp;
    private RichPopup savePopUp;
    private RichPopup popupDml;
    private RichSelectOneChoice municipios;
    private RichPopup popupCancelar;
    private RichDialog cancelarTransaccion;
    PropertiesReader properties;
    private String accion;

    /**
     * Constructor de la case, en el cual obtenemos el archivo de poperties para
     * obtener los mensajes
     * @author Antuan Ya�ez
     * @version 1.0
     */
    public CataUsosSuelosBean() {
        try {
            properties =
                    new PropertiesReader("/mx/tgc/view/ViewControllerBundle.properties", CataUsosSuelosBean.class);
        } catch (IOException ioe) {
            // TODO: Add catch code
            ioe.printStackTrace();
        }

        if (properties == null) {
            try {
                properties =
                        new PropertiesReader("/mx/tgc/view/ViewControllerBundle.properties", CataUsosSuelosBean.class);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * M�todo que se ejecuta al preparar la p�gina, se realiza el filtrado por la
     * recaudaci�n, y se filtran los tipos de suelos padres.
     * @param lifecycleContext
     * @author Antuan Ya�ez
     * @version 1.0
     */
    public void prepareModel(LifecycleContext lifecycleContext) {
        //TODO: implementar este m�todo para obtener los datos del usuario
        //con el m�todo getInfoUser() heredado de GenericBean
        //mandar llamar el m�todo de persistencia filtrarMunicipiosXRecaudacion(String idRecaudacion)
        //utilizar los bindings (pageDef) para llamar m�todos de persitencia
        super.prepareModel(lifecycleContext);
        if (!RequestContext.getCurrentInstance().isPostback()) {
            //Obtiene la recaudaci�n base del usuario para filtrar la tabla
            String idsMunicipios = getInfoUser().get(PublicKeys.LOGIN_MUNICIPIO_ID).toString();
            if ("".equals(idsMunicipios)) {
                muestraPopup("popupRecBase");
            }
            
            BindingContainer bindings = getBindings();
            OperationBinding ob1 = bindings.getOperationBinding("filtrarMunicipiosXUsuario");
            ob1.getParamsMap().put("muniId", idsMunicipios);
            ob1.execute();

            String debug = super.getParametroGeneral("AA0008");
            if (debug == null && debug.equals("")) {
                String msj = properties.getMessage("SYS.PARAMETRO_PR0008_NO_EXISTE");
                mostrarMensaje(FacesMessage.SEVERITY_ERROR, msj);
            }
        }
    }

    /**
     * M�todo que genera el panel de b�squeda.
     * @return RichPanelGroupLayout
     * @author Antuan Ya�ez
     * @version 1.0
     */
    @Override
    public RichPanelGroupLayout getPanelBusquedaTreeTable() {
        RequestContext rc = RequestContext.getCurrentInstance();
        if (!rc.isPostback()) {
            this.setTituloPanelBusquedaTreeTable(properties.getMessage("TIT.SECCION_BUSQUEDA"));
            this.setTipPanelBusquedaTreeTable(properties.getMessage("TIP.BUSQUEDA_TREE_TABLE"));
            this.setBuscarPorTreeTable(properties.getMessage("LOV.BUSCAR_POR_TREE_TABLE"));
            this.setCriterioBusquedaTreeTable(properties.getMessage("LOV.CRITERIO_TREE_TABLE"));
            this.setParametroTreeTable(properties.getMessage("NOM.PARAM_TREE_TABLE"));
            this.setBotonBuscarTreeTable(properties.getMessage("BOTON.BUSCAR"));
            this.setColumnas(3);
            List<SelectItem> campos = new ArrayList<SelectItem>();
            campos.add(new SelectItem("Clave", "Clave"));
            campos.add(new SelectItem("Nombre", "Nombre"));
            setManagedBean("CataUsosSuelosBean");
            setCamposDeBusquedaTreeTable(campos);
        }
        return super.getPanelBusquedaTreeTable();

    }

    /**
     * M�todo que se ejecuta antes de desplegar el popUp para modificar o
     * agregar un uso de suelo.
     * @param popupFetchEvent
     * @author Antuan Ya�ez
     * @version 1.0
     */
    public void editPopupFetchListener(PopupFetchEvent popupFetchEvent) {
        String clientId = accion;
        Row row = null;
        AdfFacesContext facesCtx = AdfFacesContext.getCurrentInstance();
        Map scopeVar = facesCtx.getPageFlowScope();
        scopeVar.remove("clavePadre");
        if (clientId.contains("Insert")) {
            OperationBinding operationBinding = getBindings().getOperationBinding("CreateInsert");
            if (clientId.contains("Root")) {
                // this.getSecaPadre().setRendered(false);
                operationBinding.execute();
                //this.setRecaudacion();
                AttributeBinding ab = (AttributeBinding)getBindings().getControlBinding("Nivel");
                ab.setInputValue(1);
            } else {
                row = obtenerFilaSeleccionadaTT();
                Integer nivelHijo = Integer.parseInt(row.getAttribute("Nivel").toString()) + 1;
                operationBinding.execute();
                AttributeBinding ab2 = (AttributeBinding)getBindings().getControlBinding("PrGiroIdentificador");
                ab2.setInputValue(Integer.parseInt(row.getAttribute("Identificador").toString()));
                AttributeBinding ab3 = (AttributeBinding)getBindings().getControlBinding("PrGiroSerie");
                ab3.setInputValue(Integer.parseInt(row.getAttribute("Serie").toString()));
                AttributeBinding ab = (AttributeBinding)getBindings().getControlBinding("Nivel");
                ab.setInputValue(Integer.parseInt(nivelHijo.toString()));

                //Obtenemos el uso de suelo padre
                String usosSulosPadres = "";
                /*
             * Obtenemos en uso de suelo padre para obtener su clave y nombre.
             * usosSulosPadres = row.getAttribute("Clave").toString()+" "+ row.getAttribute("Nombre");
             */
                usosSulosPadres =
                        getClavesPadres(Long.parseLong(row.getAttribute("Identificador").toString()), Integer.parseInt(row.getAttribute("Serie").toString()));
                scopeVar.put("clavePadre", usosSulosPadres);
            }
            String muniId = this.setMunicipio();
            AttributeBinding ab4 = (AttributeBinding)getBindings().getControlBinding("PcMuniIdentificador");
            ab4.setInputValue(Integer.parseInt(muniId));
        } else if (clientId.contains("Edit")) {
            row = obtenerFilaSeleccionadaTT();
            filtrarUsosSuelos(row);

            String usosSulosPadres = "";

            //Obtenemos el uso de suelo padre en caso de que lo tenga           
            if (row!=null) {
                //usosSulosPadres = rowPadre.getAttribute("Clave").toString()+" "+ rowPadre.getAttribute("Nombre");
                if (row.getAttribute("Serie")!=null || row.getAttribute("Serie")!=null){
                    usosSulosPadres =
                            getClavesPadres(Long.parseLong(row.getAttribute("Identificador").toString()),
                                            Integer.parseInt(row.getAttribute("Serie").toString()));
                    scopeVar.put("clavePadre", usosSulosPadres);
                }                
            }
        }
    }
    
    public String setMunicipio() {
        FacesContext fc = FacesContext.getCurrentInstance();
        ELContext elctx = fc.getELContext();
        ExpressionFactory elFactory = fc.getApplication().getExpressionFactory();
        DCBindingContainer bc =
            (DCBindingContainer)elFactory.createValueExpression(elctx, "#{bindings}", DCBindingContainer.class).getValue(elctx);
        DCIteratorBinding it = bc.findIteratorBinding("AaUsuariosMunicipiosVVO1Iterator");
        for (int i = 0; i < it.getViewObject().getEstimatedRowCount(); i++) {
            Row r = it.getRowAtRangeIndex(i);
            String clave = (String)r.getAttribute("ClaveMunicipio");
            String seleccion = municipios.getValue().toString();
            if (seleccion.equals(clave)) {
                oracle.binding.AttributeBinding PcMuniIdentificador =
                    (oracle.binding.AttributeBinding)getBindings().getControlBinding("PcMuniIdentificador");
                PcMuniIdentificador.setInputValue(r.getAttribute("Identificador"));
                return r.getAttribute("Identificador").toString();
            }
        }
        return "";
    }

    /**
     * M�todo que consulta persistencia para obtener la claves de los padres.
     * @param id Identificador del uso de suelo del cual se obtendr�n las claves
     * y nombres padres
     * @param serie Serie del uso de suelo del cual se obtendr�n las claves
     * y nombres padres
     * @return Clave Nombre de uso suelo concatenados
     * @author Antuan Ya�ez
     * @version 1.0
     */
    private String getClavesPadres(Long id, Integer serie) {
        String usosSulosPadres = "";
        BindingContainer bindings = getBindings();
        OperationBinding ob = bindings.getOperationBinding("getGirosPadres");

        //Filtra la tabla de los padres
        ob.getParamsMap().put("id", id);
        ob.getParamsMap().put("serie", serie);
        usosSulosPadres = (String)ob.execute();
        return usosSulosPadres;
    }

    /**
     * M�todo que filtra el uso suelo que ser� modificado
     * @param row
     * @author Antuan Ya�ez
     * @version 1.0
     */
    private void filtrarUsosSuelos(Row row) {
        OperationBinding op = (OperationBinding)this.getBindings().getControlBinding("filtrarGiro");
        op.getParamsMap().put("serie", row.getAttribute("Serie").toString());
        op.getParamsMap().put("id", row.getAttribute("Identificador").toString());
        op.execute();
    }

    /**
     * M�todo que verifica si se est� realizando un nuevo registro padre o hijo,
     * o bien se est� realizando la modificaci�n de un uso suelo.
     * @param actionEvent
     * @author Antuan Ya�ez
     * @version 1.0
     */
    public void realizarValidaciones(ActionEvent actionEvent) {
        String clientId = actionEvent.getComponent().getId();
        if (clientId.contains("Insert")) {
            if (!clientId.contains("Root")) {
                Row row = obtenerFilaSeleccionadaTT();
                if (row == null) {
                    this.mostrarMensaje(FacesMessage.SEVERITY_WARN, properties.getMessage("SYS.REGISTRO"));
                    return;
                }
            }
        } else if (clientId.contains("Edit")) {
            Row row = obtenerFilaSeleccionadaTT();
            if (row == null) {
                this.mostrarMensaje(FacesMessage.SEVERITY_WARN, properties.getMessage("SYS.REGISTRO"));
                return;
            }
            
            JUCtrlHierNodeBinding nodo = obtenerNodoSeleccionadoTT();         
            if (nodo.getChildren()!=null){
                mostrarMensaje(FacesMessage.SEVERITY_WARN, properties.getMessage("SYS.SEGMENTO_TIENE_HIJOS_MOD"));
                return;
            }

        }
        accion = clientId;
        muestraPopup();
    }

    /**
     * M�todo que muestra el popUp para modificar o insertar un nuevo uso de suelo..
     * @return
     * @author Antuan Ya�ez
     * @version 1.0
     */
    public String muestraPopup() {
        try {
            ExtendedRenderKitService erks =
                Service.getRenderKitService(FacesContext.getCurrentInstance(), ExtendedRenderKitService.class);
            StringBuilder strb =
                new StringBuilder("AdfPage.PAGE.findComponent(\"" + this.popupDml.getClientId(FacesContext.getCurrentInstance()) +
                                  "\").show();");
            erks.addScript(FacesContext.getCurrentInstance(), strb.toString());
        } catch (Exception e) {
            FacesContext context = FacesContext.getCurrentInstance();
            context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Error en operaci�n", null));
        }
        return null;
    }

    /**
     * M�todo en el cual se confirma la transacci�n y actualiza la visa de usos suelos.
     * @param actionEvent
     * @author Antuan Ya�ez
     * @version 1.0
     */
    public void aceptarListener(ActionEvent actionEvent) {
        commitOperation();
        mostrarMensaje(FacesMessage.SEVERITY_INFO, properties.getMessage("SYS.OPERACION_EXITOSA"));
        ocultaPopup();
        //Anterior: "Inserci�n de registro exitosa.");
        refreshTreeIterator("PrGirosVO1Iterator");
    }

    /**
     * M�todo en el cual se realiza el rollback de la transacci�n y oculta el popUp
     * @param dialogEvent
     * @author Antuan Ya�ez
     * @version 1.0
     */
    public void cancelarDialogListener(DialogEvent dialogEvent) {
        rollbackOperation();
        ocultaPopup();
    }

    /**
     * M�todo que realiza la confirmaci�n de la transacci�n.
     * @author Antuan Ya�ez
     * @version 1.0
     */
    private void commitOperation() {
        OperationBinding operationBinding = getBindings().getOperationBinding("Commit");
        operationBinding.execute();
    }

    /**
     * M�todo que realiza el rollback de la transacci�n.
     * @author Antuan Ya�ez
     * @version 1.0
     */
    private void rollbackOperation() {
        OperationBinding operationBinding = getBindings().getOperationBinding("Rollback");
        operationBinding.execute();
    }

    /**
     * M�todo que oculta el popUp para modificar o insertar un nuevo uso de suelo.
     * @return
     * @author Antuan Ya�ez
     * @version 1.0
     */
    public String ocultaPopup() {
        try {
            ExtendedRenderKitService erks =
                Service.getRenderKitService(FacesContext.getCurrentInstance(), ExtendedRenderKitService.class);
            StringBuilder strb =
                new StringBuilder("AdfPage.PAGE.findComponent(\"" + this.popupDml.getClientId(FacesContext.getCurrentInstance()) +
                                  "\").hide();");
            erks.addScript(FacesContext.getCurrentInstance(), strb.toString());
        } catch (Exception e) {
            FacesContext context = FacesContext.getCurrentInstance();
            context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Error en operaci�n", null));
        }
        return null;
    }

    /**
     * M�todo en el cual se valida si el usuario dio ok al confirmar el borrado
     * del registro para confirmar la acci�n.
     * @param dialogEvent
     * @author Antuan Ya�ez
     * @version 1.0
     */
    public void borrarDialogListener(DialogEvent dialogEvent) {
        if (dialogEvent.getOutcome().name().equals("ok")) {
            //RowTreeTable rowTreeTable = this.getRowTreeTable();
            Row row=obtenerFilaSeleccionadaTT();
            if (row == null) {
                mostrarMensaje(null, properties.getMessage("SYS.REGISTRO"));
                return;
            }
            
            JUCtrlHierNodeBinding nodo=obtenerNodoSeleccionadoTT();
            if (nodo.getChildren()!=null){
                mostrarMensaje(FacesMessage.SEVERITY_WARN, properties.getMessage("SYS.SEGMENTO_TIENE_HIJOS"));
                return;
            }

            AttributeBinding selectedRow = (AttributeBinding)this.getBindings().getControlBinding("selectedRow");
            selectedRow.setInputValue("false");
            BindingContainer bindings = getBindings();

            row.remove();

            OperationBinding operationBinding2 = bindings.getOperationBinding("Commit");
            operationBinding2.execute();

            if (!operationBinding2.getErrors().isEmpty()) {
                OperationBinding operationBinding3 = bindings.getOperationBinding("Rollback");
                operationBinding3.execute();
                FacesContext fc = FacesContext.getCurrentInstance();
                String msj = properties.getMessage("SYS.REGISTRO_UTILIZADO");
                fc.addMessage(null, new FacesMessage(msj));
            } else {
                commitOperation();
                refreshTable();
                mostrarMensaje(FacesMessage.SEVERITY_INFO, properties.getMessage("SYS.OPERACION_EXITOSA"));
            }

        }
    }

    public void setPopupDml(RichPopup popupDml) {
        this.popupDml = popupDml;
    }

    public RichPopup getPopupDml() {
        return popupDml;
    }

    public void setPopupCancelar(RichPopup popupCancelar) {
        this.popupCancelar = popupCancelar;
    }

    public RichPopup getPopupCancelar() {
        return popupCancelar;
    }

    public BindingContainer getBindings() {
        return BindingContext.getCurrent().getCurrentBindingsEntry();
    }

    public void setConfirmaPopUp(RichPopup confirmaPopUp) {
        this.confirmaPopUp = confirmaPopUp;
    }

    public RichPopup getConfirmaPopUp() {
        return confirmaPopUp;
    }

    public void setSavePopUp(RichPopup savePopUp) {
        this.savePopUp = savePopUp;
    }

    public RichPopup getSavePopUp() {
        return savePopUp;
    }


    public void setCancelarTransaccion(RichDialog cancelarTransaccion) {
        this.cancelarTransaccion = cancelarTransaccion;
    }

    public RichDialog getCancelarTransaccion() {
        return cancelarTransaccion;
    }

    /**
     * Fecha:08/05/2015
     * Origen:RE - Catastro - MR0017_1 - DGO - Caracter�sticas valuaci�n - Al cancelar un alta y volver a entrar a esa secci�n. El sistema muestra los valores de alta cancelada
     * Autor:Brenda Gomez.
     * */
    public void cerrarPopupDml(ActionEvent actionEvent) {
        popupDml.cancel();
        resetComponent(popupDml);
        BindingContainer bindings = getBindings();
        OperationBinding operationBinding2 = bindings.getOperationBinding("Rollback");
        operationBinding2.execute();

    }

     /**
      * Fecha:12/05/2015
      * Origen:RE - Catastro - MR0017_1 - DGO - Cat�logo de segmentos catastrales - Se desaparecen los botones al querer agregar un hijo de tercer nivel.
      * Autor:Brenda Gomez.
      * */
     public Row obtenerFilaSeleccionadaTT() {
         Row row = null;
         JUCtrlHierNodeBinding node = obtenerNodoSeleccionadoTT();
         if (node != null) {
             row = node.getRow();
         }
         return row;
     }

     /**
      * Fecha:12/05/2015
      * Origen:RE - Catastro - MR0017_1 - DGO - Cat�logo de segmentos catastrales - Se desaparecen los botones al querer agregar un hijo de tercer nivel.
      * Autor:Brenda Gomez.
      * */
     public JUCtrlHierNodeBinding obtenerNodoSeleccionadoTT() {
         RowKeySet rks = getTreeTable().getSelectedRowKeys();
         JUCtrlHierNodeBinding nodo = null;
         if (rks != null) {
             CollectionModel treeModel = (CollectionModel)this.getTreeTable().getValue();
             JUCtrlHierBinding treeBinding = (JUCtrlHierBinding)treeModel.getWrappedData();
             List firstSet = (List)rks.iterator().next();
             nodo = treeBinding.findNodeByKeyPath(firstSet);
         }
         return nodo;
     }

    public void setMunicipios(RichSelectOneChoice municipios) {
        this.municipios = municipios;
    }

    public RichSelectOneChoice getMunicipios() {
        return municipios;
    }
}
