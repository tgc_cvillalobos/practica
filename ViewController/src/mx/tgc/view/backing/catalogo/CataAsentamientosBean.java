package mx.tgc.view.backing.catalogo;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.validator.ValidatorException;

import mx.tgc.utilityfwk.resource.PropertiesReader;
import mx.tgc.utilityfwk.resource.PublicKeys;
import mx.tgc.utilityfwk.view.managed.GenericBean;
import mx.tgc.utilityfwk.view.search.SearchComponent;
import mx.tgc.utilityfwk.view.search.Searchable;

import oracle.adf.controller.v2.context.LifecycleContext;
import oracle.adf.model.BindingContext;
import oracle.adf.model.binding.DCBindingContainer;
import oracle.adf.view.rich.component.rich.RichDialog;
import oracle.adf.view.rich.component.rich.RichPopup;
import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.context.AdfFacesContext;
import oracle.adf.view.rich.event.DialogEvent;
import oracle.adf.view.rich.event.PopupCanceledEvent;
import oracle.adf.view.rich.event.PopupFetchEvent;

import oracle.binding.AttributeBinding;
import oracle.binding.BindingContainer;
import oracle.binding.OperationBinding;

import oracle.jbo.Row;
import oracle.jbo.uicli.binding.JUCtrlHierNodeBinding;

import org.apache.myfaces.trinidad.context.RequestContext;
import org.apache.myfaces.trinidad.render.ExtendedRenderKitService;
import org.apache.myfaces.trinidad.util.Service;


public class CataAsentamientosBean extends GenericBean implements Searchable {
  private RichPopup cancelarPopUp;
  private RichDialog confirmaPopUp;
  private RichPopup borrarPopUp;
  private DCBindingContainer bindingContainer;
  private RichTable table;
  PropertiesReader properties;
  private RichPopup popupDml;

  /**
   * This is the default constructor (do not remove).
   */
  public CataAsentamientosBean() {
    if (properties == null) {
      try {
        properties =
            new PropertiesReader("/mx/tgc/view/ViewControllerBundle.properties",
                                 CataAsentamientosBean.class);
      } catch (Exception e) {
        e.printStackTrace();
      }
    }
  }


    /**se ejecuta el m�todo de persistencia para filtrar los segmentos
     * catastrales por recaudaci�n base del usuario firmado, mandando llamar
     * primero el m�todo getInfoUser para obtener la recaudaci�n base del
     * usuario y despu�s con el id de la recaudaci�n mandar llamar el m�todo
     * filtrarMunicipiosXRecaudacion (CataCaractAsentamientosAMImpl).
     * @param lifecycleContext
     */
    @Override
    public void prepareModel(LifecycleContext lifecycleContext) {
        super.prepareModel(lifecycleContext);
        RequestContext rc = RequestContext.getCurrentInstance();
        if (!rc.isPostback()) {
            //Obtiene la recaudaci�n base del usuario para filtrar la tabla
            String idsMunicipios = getInfoUser().get(PublicKeys.LOGIN_MUNICIPIO_ID).toString();
            AdfFacesContext facesCtx = AdfFacesContext.getCurrentInstance();
            Map scopeVar = facesCtx.getPageFlowScope();
            String s = (String)scopeVar.get("catalogo");
            String munSel = (String)scopeVar.get("municipioSel");

            if (munSel != null && !"".equals(munSel)) {
                idsMunicipios = munSel;
            }

            if ("".equals(idsMunicipios)) {
                muestraPopup("popupRecBase");
            }

            BindingContainer bindings = getBindings();
            OperationBinding ob = bindings.getOperationBinding("filtrarMunicipiosXUsuario");
            ob.getParamsMap().put("muniId", idsMunicipios);
            ob.execute();

            OperationBinding filtrarActivos = bindings.getOperationBinding("filtrarActivos");
            filtrarActivos.getParamsMap().put("view", "PcAsentamientosVO3");

            if (s == null) {
                scopeVar.put("catalogo", "true");
                filtrarActivos.getParamsMap().put("filtrar", false);
            } else {
                scopeVar.put("catalogo", "false");
                filtrarActivos.getParamsMap().put("filtrar", true);
            }
            filtrarActivos.execute();
            //Consulta de par�metro PR0009 que contendr� el valor que llevar� la
            //etiqueta configurable (Colonia o asentamiento).
            String etiqueta = super.getParametroGeneral("PR0009");
            if (etiqueta != null && !etiqueta.equals("")) {
                if (etiqueta.equals("ASENTAMIENTO") || etiqueta.equals("COLONIA")) {
                    super.getPageFlowScope().put("PR0009", etiqueta);
                } else {
                    super.getPageFlowScope().put("PR0009", "ASENTAMIENTO");
                    String msj = properties.getMessage("SYS.PARAMETRO_PR0009_NO_EXISTE");
                    mostrarMensaje(FacesMessage.SEVERITY_ERROR, msj);
                }
            } else {
                super.getPageFlowScope().put("PR0009", "ASENTAMIENTO");
                String msj = properties.getMessage("SYS.PARAMETRO_PR0009_NO_EXISTE");
                mostrarMensaje(FacesMessage.SEVERITY_ERROR, msj);
            }
            String debug = super.getParametroGeneral("AA0008");
            if (debug == null && debug.equals("")) {
                String msj = properties.getMessage("SYS.PARAMETRO_PR0008_NO_EXISTE");
                mostrarMensaje(FacesMessage.SEVERITY_ERROR, msj);
            }
        }
    }

  private DCBindingContainer getBindingContainer() {
    return (DCBindingContainer)BindingContext.getCurrent().bindingContainers().toArray()[0];
  }


  /**
   * M�todo que sube al PageFlowScope los valores que regresar� el cat�logo
   * teniendo comportamiento de pop up.
   * @return
   * @autor Ang�lica Ledezma
   * @since 14/09/2011
   */
  public String subirValores() {
    Iterator iter = getTable().getSelectedRowKeys().iterator();
    if (iter != null && iter.hasNext()) {
      JUCtrlHierNodeBinding rowData =
        (JUCtrlHierNodeBinding)getTable().getSelectedRowData();
      Row row = rowData.getCurrentRow();
      AdfFacesContext facesCtx = AdfFacesContext.getCurrentInstance();
      Map scopeVar = facesCtx.getPageFlowScope();
      scopeVar.put("idAsentamiento", row.getAttribute("Identificador"));
      scopeVar.put("idLocaAsentamiento",
                   row.getAttribute("PcLocaIdentificador"));
      return "exit";
    } else {
      String msj =
        properties.getMessage("PC_ASENTAMIENTOS.MANDATORY.ASENTAMIENTO");
      mostrarMensaje(FacesMessage.SEVERITY_INFO, msj);
      return null;
    }
  }

  /**
   * Este m�todo sirve para insertar un registro de la tabla
   * PC_ASENTAMIENTOS, es necesario ver el manual
   * de creaci�n maestro-detalle para ver la implementaci�n del m�todo.
   *
   * @return
   */
  public String insertarRegistro() {
    BindingContainer bindings = getBindings();
    OperationBinding ob = bindings.getOperationBinding("Commit");
    ob.execute();
    if (!ob.getErrors().isEmpty()) {
      FacesContext fc = FacesContext.getCurrentInstance();
      String msj = properties.getMessage("SYS.OPERACION_FALLIDA");
      fc.addMessage(null, new FacesMessage(msj));
      return null;
    } else {
      mostrarMensaje(FacesMessage.SEVERITY_INFO,
                     properties.getMessage("SYS.OPERACION_EXITOSA"));
      ocultaPopup();
      return null;
    }
  }

  /**
   * Este m�todo sirve para actualizar un registro de la tabla
   * PC_ASENTAMIENTOS, es necesario ver el manual
   * de creaci�n maestro-detalle para ver la implementaci�n del m�todo.
   *
   * @return
   */
  public String modificarRegistro() {
    return null;
  }

  /**
   * Este m�todo sirve para borrar un registro de la tabla
   * PC_ASENTAMIENTOS, es necesario ver el manual
   * de creaci�n maestro-detalle para ver la implementaci�n del m�todo.
   *
   * @return
   */
  public String borrarRegistro() {
    BindingContainer bindings = getBindings();
    OperationBinding ob = bindings.getOperationBinding("Delete");
    ob.execute();

    OperationBinding operationBinding2 =
      bindings.getOperationBinding("Commit");
    operationBinding2.execute();
    System.out.println(ob.getErrors());
    ob.getErrors();
    if (!ob.getErrors().isEmpty() ||
        !operationBinding2.getErrors().isEmpty()) {
      OperationBinding operationBinding3 =
        bindings.getOperationBinding("Rollback");
      operationBinding3.execute();
      FacesContext fc = FacesContext.getCurrentInstance();
      String msj = properties.getMessage("SYS.TIENE_HIJOS");
      fc.addMessage(null, new FacesMessage(msj));
    } else {
      mostrarMensaje(FacesMessage.SEVERITY_INFO,
                     properties.getMessage("SYS.OPERACION_EXITOSA"));
      return null;
    }

    return null;
  }

  /**
   *Este m�todo se sobreescribe para implementar la b�squeda custom, esto se
   * hace autom�ticamente.
   * @return
   */

  public List<SearchComponent> prepararBusqueda() {
    this.setTituloPanelBusqueda(properties.getMessage("TIT.SECCION_BUSQUEDA"));
    this.setTipPanelBusqueda(properties.getMessage("TIP.BUSQUEDA_CUSTOM"));
    this.setBotonBuscar(properties.getMessage("BOTON.BUSCAR"));
    List<SearchComponent> l = new ArrayList<SearchComponent>();
    //Consulta de par�metro PR0009 que contendr� el valor que llevar� la
    //etiqueta configurable (Colonia o asentamiento).
    String etiqueta = (String)getPageFlowScope().get("PR0009");

    SearchComponent sc1 = new SearchComponent("CataAsentamientosBean");
    sc1.setComponente(SearchComponent.INPUT_TEXT);
    sc1.setAtributo("ClaveAsentamiento");
    sc1.setSize(10);
    if (etiqueta.equals("ASENTAMIENTO")) {
      sc1.setLabel("Clave asentamiento:");
    } else if (etiqueta.equals("COLONIA")) {
      sc1.setLabel("Clave colonia:");
    }
    sc1.setId("txtClave");
    sc1.setSize(10);

    SearchComponent sc2 = new SearchComponent("CataAsentamientosBean");
    sc2.setComponente(SearchComponent.SELECT_ONE_CHOICE);
    sc2.setAtributo("TipoAsentamiento");
    sc2.setSize(10);
    if (etiqueta.equals("ASENTAMIENTO")) {
      sc2.setLabel("Tipo de asentamiento:");
    } else if (etiqueta.equals("COLONIA")) {
      sc2.setLabel("Tipo de colonia:");
    }
    sc2.setId("socTipoAsentamiento");
    sc2.setDominio("PC_ASENTAMIENTOS.TIPO_ASENTAMIENTO");


    SearchComponent sc3 = new SearchComponent("CataAsentamientosBean");
    sc3.setComponente(SearchComponent.INPUT_TEXT);
    sc3.setAtributo("NombreAsentamiento");
    sc3.setSize(10);
    if (etiqueta.equals("ASENTAMIENTO")) {
      sc3.setLabel("Nombre de asentamiento:");
    } else if (etiqueta.equals("COLONIA")) {
      sc3.setLabel("Nombre de colonia:");
    }
    sc3.setId("txtNombreAsentamiento");
    sc3.setSize(20);

    l.add(sc1);
    l.add(sc2);
    l.add(sc3);
    
    AdfFacesContext facesCtx = AdfFacesContext.getCurrentInstance();
    Map scopeVar = facesCtx.getPageFlowScope();
    String s = (String)scopeVar.get("catalogo");
    Boolean catalogo=false;
    if(s!=null){
          catalogo=Boolean.parseBoolean(s);
    }
    //Solo agrega la busqueda de estatus cuando el catalogo
    //sea mandado llamar como catalogo
    if(catalogo){
       SearchComponent sc4 = new SearchComponent("CataAsentamientosBean");
       sc4.setComponente(SearchComponent.SELECT_ONE_CHOICE);
       sc4.setAtributo("Estatus");
       sc4.setLabel("Estatus:");
       sc4.setId("socEstatus");
       sc4.setDominio("PC_ASENTAMIENTOS.ESTATUS");
       l.add(sc4);
      }
    
    return l;
  }

  public void cancelAction(DialogEvent dialogEvent) {
    if (dialogEvent.getOutcome() == DialogEvent.Outcome.ok) {
      BindingContainer bindings = getBindings();
      OperationBinding operationBinding2 =
        bindings.getOperationBinding("Rollback");
      operationBinding2.execute();
      AttributeBinding op1 =
        (AttributeBinding)getBindings().getControlBinding("modifica1");
      op1.setInputValue("false");
    }
  }

  public void setCancelarPopUp(RichPopup cancelarPopUp) {
    this.cancelarPopUp = cancelarPopUp;
  }

  public RichPopup getCancelarPopUp() {
    return cancelarPopUp;
  }

  public BindingContainer getBindings() {
    return BindingContext.getCurrent().getCurrentBindingsEntry();

  }

  public void setConfirmaPopUp(RichDialog confirmaPopUp) {
    this.confirmaPopUp = confirmaPopUp;
  }

  public RichDialog getConfirmaPopUp() {
    return confirmaPopUp;
  }

  public void setBorrarPopUp(RichPopup borrarPopUp) {
    this.borrarPopUp = borrarPopUp;
  }

  public RichPopup getBorrarPopUp() {
    return borrarPopUp;
  }

  public String cancelAction() {
    // Add event code here...
    return null;
  }

  public void setBindingContainer(DCBindingContainer bindingContainer) {
    this.bindingContainer = bindingContainer;
  }

  public void setTable(RichTable table) {
    this.table = table;
  }

  public RichTable getTable() {
    return table;
  }

  public String muestraPopup() {
    try {
      ExtendedRenderKitService erks =
        Service.getRenderKitService(FacesContext.getCurrentInstance(),
                                    ExtendedRenderKitService.class);
      StringBuilder strb =
        new StringBuilder("AdfPage.PAGE.findComponent(\"" + this.popupDml.getClientId(FacesContext.getCurrentInstance()) +
                          "\").show();");
      erks.addScript(FacesContext.getCurrentInstance(), strb.toString());
    } catch (Exception e) {
      FacesContext context = FacesContext.getCurrentInstance();
      context.addMessage(null,
                         new FacesMessage(FacesMessage.SEVERITY_INFO, "Error en operaci�n",
                                          null));
    }
    return null;
  }

  public void muestraPopListener(ActionEvent actionEvent) {
    muestraPopup();
  }

  public void setPopupDml(RichPopup popupDml) {
    this.popupDml = popupDml;
  }

  public RichPopup getPopupDml() {
    return popupDml;
  }

  public void formaPopupFetchListener(PopupFetchEvent popupFetchEvent) {
    AdfFacesContext fc = AdfFacesContext.getCurrentInstance();
    String clientId = popupFetchEvent.getLaunchSourceClientId();
    Map pfs = fc.getPageFlowScope();
    String operacion = pfs.get("operacion").toString();
    if (operacion != null && operacion.equals("nuevo")) {
      OperationBinding operationBinding =
        getBindings().getOperationBinding("CreateInsert");
      operationBinding.execute();
    }
    else if(operacion != null && operacion.equals("modificar")){
        AttributeBinding claveAsentamiento=(AttributeBinding)this.getBindings().getControlBinding("ClaveAsentamiento");
        if(claveAsentamiento!=null){
           this.getPageFlowScope().put("claveAsentamiento", claveAsentamiento.getInputValue());
        }
      
    }
    
  }

  public String ocultaPopup() {
    try {
      ExtendedRenderKitService erks =
        Service.getRenderKitService(FacesContext.getCurrentInstance(),
                                    ExtendedRenderKitService.class);
      StringBuilder strb =
        new StringBuilder("AdfPage.PAGE.findComponent(\"" + this.popupDml.getClientId(FacesContext.getCurrentInstance()) +
                          "\").hide();");
      erks.addScript(FacesContext.getCurrentInstance(), strb.toString());
    } catch (Exception e) {
      FacesContext context = FacesContext.getCurrentInstance();
      context.addMessage(null,
                         new FacesMessage(FacesMessage.SEVERITY_INFO, "Error en operaci�n",
                                          null));
    }
    return null;
  }
    /**
     * Valida que la clave asentamiento no se encuentre asignada a un municipio.
     *
     * @param facesContext
     * @param uIComponent
     * @param object
     * @author Juan Carlos Olivas
     * @date 21/04/2014
     */
    public void claveAsentamientoValidator(FacesContext facesContext,
                                           UIComponent uIComponent,
                                           Object object) {
        Map res;
        if(object!=null){
            BindingContainer bindings = getBindings();
            OperationBinding ob =
            bindings.getOperationBinding("validaClaveAsentamiento");
            ob.getParamsMap().put("claveAsentamiento", object.toString());
            res=(Map)ob.execute();
            //si la claveAsentamiento ya existe
            if(res.containsKey("resultado")){
                
                AdfFacesContext fc = AdfFacesContext.getCurrentInstance();
                Map pfs = fc.getPageFlowScope();
                String operacion = pfs.get("operacion").toString();
                //Si es una actualizacion
                if(operacion != null && operacion.equals("modificar")){
                
                   String claveAsentamiento= this.getPageFlowScope().get("claveAsentamiento").toString();
                   //se valida que no se utilice una claveAsentamiento ya existente para actualizar
                   if(!claveAsentamiento.equals(object.toString())){
                       throw new ValidatorException(new FacesMessage(
                               FacesMessage.SEVERITY_ERROR,
                               "La clave de asentamiento "+object.toString()+" esta asignada al municipio "+res.get("municipio")+".",
                                                                            null));
                   }
                }else{
                    //Si la clave existe y no es una actualizacion, 
                    //se esta intentando insertar una clave duplicada
                    throw new ValidatorException(new FacesMessage(
                            FacesMessage.SEVERITY_ERROR,
                            "La clave de asentamiento "+object.toString()+" esta asignada al municipio "+res.get("municipio")+".",
                                                                         null));
                }    
            }
        }
    }

    public void cerrarPopupEscape(PopupCanceledEvent popupCanceledEvent) {
        BindingContainer bindings = getBindings();
        OperationBinding operationBinding2 =
          bindings.getOperationBinding("Rollback");
        operationBinding2.execute();
        return;
    }
    
    public void cerrarPopupDml(ActionEvent actionEvent){
        popupDml.cancel();
        resetComponent(popupDml);
        BindingContainer bindings = getBindings();
        OperationBinding operationBinding2 =
          bindings.getOperationBinding("Rollback");
        operationBinding2.execute();
        
    }
}
